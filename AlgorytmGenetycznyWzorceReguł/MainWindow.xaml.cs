﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using AlgorytmGenetycznyWzorceReguł.AlgotytmModel;
using AlgorytmGenetycznyWzorceReguł.Controller;
using System.ComponentModel;
using System.Collections.ObjectModel;
using AlgorytmGenetycznyWzorceReguł.RuleEvaluation;


namespace AlgorytmGenetycznyWzorceReguł
{

    public partial class MainWindow : Window
    {
        public int populationRange { get; set; }

        public int stopCondition { get; set; }

        public int mutationProbability { get; set; }

        public int crossoverProbability { get; set; }

        public string CheckFileName = "";

        public string ValFileName = "";

        public string DtFileName = "";


        public List<string> selectedItemsIndex = new List<string>();
        public List<String> Rules = new List<string>();
        public List<String> ChoosenRules = new List<String>();

        public List<Chromosome> ChoosenChromosomeRules = new List<Chromosome>();
        public double[] evaluateValue;


        DecisionTable decisionTable = new DecisionTable();
        Population bestPopulation;
        Algorytm algorytm = new Algorytm();

        EvaluationRules evaluationRules = new EvaluationRules();

        public double[] RulesMark;


        public MainWindow()
        {
            InitializeComponent();
        }



        private void MiNew_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MiOpenFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            string fileName = "";

            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                fileName = dlg.FileName;
            }

            decisionTable.init(fileName);
            bestPopulation = new Population(decisionTable.ListOfPossibilities, populationRange);

        }

        private void MiSaveFile_Click(object sender, RoutedEventArgs e)
        {

        }

        private void MiRun_Click(object sender, RoutedEventArgs e)
        {
            if (TbPopulationRange != null & TbIterations != null & TbMutationProbability != null & TbCrossoverProbability != null)
            {
                populationRange = Convert.ToInt32(TbPopulationRange.Text);
                stopCondition = Convert.ToInt32(TbIterations.Text);
                mutationProbability = Convert.ToInt32(TbMutationProbability.Text);
                crossoverProbability = Convert.ToInt32(TbCrossoverProbability.Text);

                bestPopulation = algorytm.Run(decisionTable, populationRange, stopCondition, mutationProbability, crossoverProbability);
            }
            else
                MessageBox.Show("Błędne dane");
        }

        private void MiExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }


        private void TbCrossoverProbability_TextChanged(object sender, TextChangedEventArgs e)
        {

            if (TbCrossoverProbability.Text != "")
            {
                crossoverProbability = Convert.ToInt32(TbCrossoverProbability.Text);
            }
        }

        private void TbMutationProbability_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TbMutationProbability.Text != "")
            {
                mutationProbability = Convert.ToInt32(TbMutationProbability.Text);
            }
        }

        private void TbPopulationRange_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TbPopulationRange.Text != "")
            {
                populationRange = Convert.ToInt32(TbPopulationRange.Text);
            }
        }

        private void TbIterations_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TbIterations.Text != "")
            {
                stopCondition = Convert.ToInt32(TbIterations.Text);
            }
        }

        private void BtnGenereteRules_Click(object sender, RoutedEventArgs e)
        {
            if (DtFileName != null & CheckFileName != null & ValFileName != null)
            {
                decisionTable.initThreeSetsFromFile(DtFileName, CheckFileName, ValFileName);
            }
            else if (DtFileName == null)
            {
                MessageBox.Show("Pusty zestaw danych.");
            }
            else if (CheckFileName == null)
            {
                MessageBox.Show("Pusty zestaw treningowy.");
            }
            else if (ValFileName == null)
            {
                MessageBox.Show("Pusty zestaw walidacyjny.");
            }
            else
            {
                MessageBox.Show("Błąd wczytywania danych.");
            }
        }

        private void BtnOpenDtFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                DtFileName = dlg.FileName;
            }
        }

        private void BtnOpenCheckFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                CheckFileName = dlg.FileName;
            }
        }

        private void BtnOpenValFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                ValFileName = dlg.FileName;
            }
        }

        private void BtnShowRules_Click(object sender, RoutedEventArgs e)
        {
            LbRules.ItemsSource = algorytm.Rules;
        }

        private void LbRules_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string a = LbRules.SelectedIndex.ToString();
            selectedItemsIndex.Add(a);

            ChoosenChromosomeRules.Add(bestPopulation.CurrentPopulation[int.Parse(a)]);

            LbChoosenRules.Items.Add(LbRules.SelectedItem.ToString());
        }

        private void BtnShowProcent_Click(object sender, RoutedEventArgs e)
        {
            //for (int i = 0; i < selectedItemsIndex.Count(); i++)
            //{
            //    for (int j = 0; j < algorytm.RulesOriginal.Count(); j++)
            //    {
            //        if (Convert.ToInt32(selectedItemsIndex[i]) == j)
            //        {
            //            ChoosenRules.Add(algorytm.RulesOriginal[j]);
            //        }
            //    }
            //}


            //for (int i = 0; i < ChoosenRules.Count(); i++)
            //{
            //    int k = bestPopulation.CurrentPopulation[0].Genes.Count();
                
            //    string seperationChar = ",";
            //    char[] charSeparators = new char[] { ',' };

            //    Chromosome chromosome = new Chromosome(k);
            //    string currentGene = "";
            //    int currentGeneNum = 0; 

            //    for (int f = 0; f < ChoosenRules[0].Length; f++)
            //    {
            //        if (ChoosenRules[i].ToString().Contains(seperationChar))
            //        {
            //            chromosome.Genes[currentGeneNum] = currentGene;
            //            currentGene = "";
            //            currentGeneNum++;
            //        }
            //        else
            //        {
            //            currentGene += ChoosenRules[f].ToString();
            //        }
            //    }
            //    chromosome.Genes[currentGeneNum] = currentGene;
                
            //    //for (int j = 0; j < k; j++)
            //    //{
            //    //    chromosome.Genes[j] = ChoosenRules[i][j].ToString();
            //    //}

            //    ChoosenChromosomeRules.Add(chromosome);
            //}


            var x = evaluationRules.evaluateRules(ChoosenChromosomeRules, decisionTable);

            evaluateValue = x;

            var xx = evaluationRules.evaluateNewRules(decisionTable, ChoosenChromosomeRules);

            int i = 0; // debug value todo: usun
        }



    }
}