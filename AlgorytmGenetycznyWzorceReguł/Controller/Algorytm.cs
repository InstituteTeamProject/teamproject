﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorytmGenetycznyWzorceReguł.AlgotytmModel;
using System.ComponentModel;

namespace AlgorytmGenetycznyWzorceReguł.Controller
{
    class Algorytm
    {
        public int MutationProbability { get; set; }

        public int CrossoverProbability { get; set; }
        public List<String> Rules = new List<string>();

        public List<String> RulesOriginal = new List<string>();

        public List<Chromosome> RulesOriginalChrom = new List<Chromosome>();


        public Population Run(DecisionTable table, int populationRange, int stopCondition, int MutationProbability, int CrossoverProbability)
        {
            bool stop = false;
            bool maxymalization = true; // zmienna to selekcji ruletki

            Crossover crossover = new Crossover();
            Selection selection = new Selection();
            int selectionIterator = 1;

            Population population = new Population(table.ListOfPossibilities, populationRange);
            Population tmpPopulation = new Population(table.ListOfPossibilities, populationRange);
            Population crossoveredTmpPopulation = new Population(table.ListOfPossibilities, populationRange);

            List<Chromosome> crossoveredChromosomes;
            int iteration = 0;

            population.init(table.ListOfPossibilities, populationRange, table.CurrentDecisionTable[0].Genes.Count());
            population.evaluate(table);

            do
            {
                if (selectionIterator == 1)
                {
                    tmpPopulation = selection.bestMomentRun(population, population.countOfObiects);
                }
                if (selectionIterator == 2)
                {
                    tmpPopulation = selection.turnamentRun(population, population.countOfObiects);
                    selectionIterator = 0;
                }
                selectionIterator++;

                //Crossover prop 98%
                for (int i = 0; i < tmpPopulation.CurrentPopulation.Count / 2; i++)
                {
                    if (i < tmpPopulation.CurrentPopulation.Count / 2)
                    {
                        crossoveredChromosomes = crossover.makeTwoPointCrossover(tmpPopulation.CurrentPopulation[i],
                            tmpPopulation.CurrentPopulation[(tmpPopulation.CurrentPopulation.Count / 2) + i]);

                        crossoveredTmpPopulation.CurrentPopulation.Add(crossoveredChromosomes[0]);
                        crossoveredTmpPopulation.CurrentPopulation.Add(crossoveredChromosomes[1]);
                    }
                    if (i == tmpPopulation.CurrentPopulation.Count / 2)
                    {
                        crossoveredChromosomes = crossover.makeTwoPointCrossover(tmpPopulation.CurrentPopulation[i],
                            tmpPopulation.CurrentPopulation[tmpPopulation.CurrentPopulation.Count]);

                        crossoveredTmpPopulation.CurrentPopulation.Add(crossoveredChromosomes[0]);
                        crossoveredTmpPopulation.CurrentPopulation.Add(crossoveredChromosomes[1]);
                    }
                }

                tmpPopulation = crossoveredTmpPopulation;
                crossoveredTmpPopulation = new Population(table.ListOfPossibilities, populationRange);

                //mutation todo probability max 2%
                Random rand = new Random();

                int probability = rand.Next(0, 100);

                if (probability < MutationProbability)
                {
                    for (int i = 0; i < tmpPopulation.CurrentPopulation.Count; i++)
                    {
                        tmpPopulation.CurrentPopulation[i].mutate(table.ListOfPossibilities);
                    }
                }


                tmpPopulation.evaluate(table);
                population = tmpPopulation;

                iteration++;

                if (iteration == stopCondition)
                {
                    stop = true;
                }

            } while (stop == false);


            int lastGen = population.CurrentPopulation[0].Genes.Count();

            for (int i = 0; i < RulesOriginalChrom.Count(); i++)
            {
                Chromosome chromosome = new Chromosome(lastGen);
                for (int j = 0; j < lastGen; j++)
                {
                   // chromosome.Genes[j] = RulesOriginalChrom[i][j].ToString();
                }
                RulesOriginalChrom.Add(chromosome);
            }
            //

            for (int i = 0; i < population.CurrentPopulation.Count(); i++)
            {
                string conclusioion = "";
                string a = "";
                string b = "";

                for (int j = 0; j < population.CurrentPopulation[i].Genes.Count() - 1; j++)
                {
                    if (j != population.CurrentPopulation[i].Genes.Count() - 2)
                    {
                        a += population.CurrentPopulation[i].Genes[j] + "^";
                    }
                    else
                    a += population.CurrentPopulation[i].Genes[j];
                                   
                    conclusioion = population.CurrentPopulation[i].Genes.Last();
                }
                a += "=>" + conclusioion;
                b = a.Replace("^", ",").Replace("=>",",");
                
                Rules.Add(a);
                RulesOriginal.Add(b);
            }

            return population;
        }
    }
}
