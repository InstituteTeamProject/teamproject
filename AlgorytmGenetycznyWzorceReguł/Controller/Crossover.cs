﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorytmGenetycznyWzorceReguł.AlgotytmModel;

namespace AlgorytmGenetycznyWzorceReguł.Controller
{
    public class Crossover
    {
        Random r;
        List<Chromosome> crossoveredChromosomes;

        public Crossover()
        {
            r = new Random();
            crossoveredChromosomes = new List<Chromosome>();
        }

        public List<Chromosome> makeTwoPointCrossover(Chromosome firstChromosome,
           Chromosome secondChromosome)
        {
            int countOfGenes = firstChromosome.Genes.Count();
            bool ok = true;

            Chromosome newFirstChromosome = new Chromosome(countOfGenes);
            Chromosome newSecondChromosome = new Chromosome(countOfGenes);

            int firstCrossoverPlace = r.Next(0, countOfGenes);
            int secondCrossoverPlace;

            do
            {
                secondCrossoverPlace = r.Next(0, countOfGenes);

                if ((secondCrossoverPlace != firstCrossoverPlace)
                    || (secondCrossoverPlace != firstCrossoverPlace + 1)
                    || (secondCrossoverPlace != firstCrossoverPlace - 1))
                {
                    ok = false;
                }
            } while (ok);

            if (secondCrossoverPlace < firstCrossoverPlace)
            {
                int tmp = firstCrossoverPlace;
                firstCrossoverPlace = secondCrossoverPlace;
                secondCrossoverPlace = tmp;
            }

            for (int i = 0; i < countOfGenes; i++)
            {
                if (i >= firstCrossoverPlace && i <= secondCrossoverPlace)
                {
                    newFirstChromosome.Genes[i] = secondChromosome.Genes[i];
                    newSecondChromosome.Genes[i] = firstChromosome.Genes[i];
                }
                else
                {
                    newFirstChromosome.Genes[i] = firstChromosome.Genes[i];
                    newSecondChromosome.Genes[i] = secondChromosome.Genes[i];
                }
            }

            crossoveredChromosomes.Add(newFirstChromosome);
            crossoveredChromosomes.Add(newSecondChromosome);

            return crossoveredChromosomes;
        }

        public List<Chromosome> makeOnePointCrossover(Chromosome firstChromosome,
           Chromosome secondChromosome)
        {
            int countOfGenes = firstChromosome.Genes.Count();

            Chromosome newFirstChromosome = new Chromosome(countOfGenes);
            Chromosome newSecondChromosome = new Chromosome(countOfGenes);

            int crossoverPlace = r.Next(0, countOfGenes);

            for (int i = 0; i < countOfGenes; i++)
            {
                if (i < crossoverPlace)
                {
                    newFirstChromosome.Genes[i] = firstChromosome.Genes[i];
                    newSecondChromosome.Genes[i] = secondChromosome.Genes[i];
                }
                else
                {
                    newFirstChromosome.Genes[i] = secondChromosome.Genes[i];
                    newSecondChromosome.Genes[i] = firstChromosome.Genes[i];
                }
            }

            crossoveredChromosomes.Add(newFirstChromosome);
            crossoveredChromosomes.Add(newSecondChromosome);

            return crossoveredChromosomes;
        }

        public List<Chromosome> makeManypointsCrossover(Chromosome firstChromosome,
            Chromosome secondChromosome)
        {
            int countOfGenes = firstChromosome.Genes.Count();

            Chromosome newFirstChromosome = new Chromosome(countOfGenes);
            Chromosome newSecondChromosome = new Chromosome(countOfGenes);
            //ToDo add probability to cross
            int numberofCrossovers = (int)(countOfGenes * 0.35);

            for (int i = 0; i < numberofCrossovers; i++)
            {
                int crossoverPlace = r.Next(0, countOfGenes);

                if (crossoverPlace == 0)
                {
                    for (int j = 0; j < countOfGenes; j++)
                    {
                        if (j == crossoverPlace)
                        {
                            newFirstChromosome.Genes[j] = firstChromosome.Genes[j + 1];
                            newFirstChromosome.Genes[j + 1] = firstChromosome.Genes[j];
                            j++;
                        }
                        else
                        {
                            newFirstChromosome.Genes[j] = firstChromosome.Genes[j];
                        }
                    }
                }
                else
                {
                    for (int j = 0; j < countOfGenes; j++)
                    {
                        if (j == crossoverPlace)
                        {
                            newFirstChromosome.Genes[j] = firstChromosome.Genes[j - 1];
                            newFirstChromosome.Genes[j - 1] = firstChromosome.Genes[j];
                        }
                        else
                        {
                            newFirstChromosome.Genes[j] = firstChromosome.Genes[j];
                        }
                    }
                }
            }

            crossoveredChromosomes.Add(newFirstChromosome);
            crossoveredChromosomes.Add(newSecondChromosome);

            return crossoveredChromosomes;
        }
    }
}
