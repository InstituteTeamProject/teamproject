﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorytmGenetycznyWzorceReguł.AlgotytmModel;

namespace AlgorytmGenetycznyWzorceReguł.Controller
{
    public class Selection
    {
        public Population turnamentRun(Population population, int countOfObiects)
        {
            Population finalPopulation = new Population(population.ListOfPossibilities, countOfObiects);
            Random r = new Random();

            int populationCount = population.CurrentPopulation.Count;

            Chromosome bestChromosome;
            Chromosome[] tmpArray = new Chromosome[4];

            for (int i = 0; i < populationCount; i++)
            {
                tmpArray[0] = population.CurrentPopulation[r.Next(0, populationCount)];
                tmpArray[1] = population.CurrentPopulation[r.Next(0, populationCount)];
                tmpArray[2] = population.CurrentPopulation[r.Next(0, populationCount)];
                tmpArray[3] = population.CurrentPopulation[r.Next(0, populationCount)];

                bestChromosome = tmpArray[0];

                for (int j = 0; j < 4; j++)
                {
                    if (tmpArray[j].Fitness > bestChromosome.Fitness)
                    {
                        bestChromosome = tmpArray[j];
                    }
                }

                finalPopulation.CurrentPopulation.Add(bestChromosome);
            }

            return finalPopulation;
        }

        //if maxymalization == true then maksymalizujemy funkcje
        //if maxymalization == false then minimalizujemy funkcje
        //public Population russianRulletteRun(Population population, bool maxymalization, int countOfObiects)
        //{
        //    Population finalPopulation = new Population(population.ListOfPossibilities, countOfObiects);
        //    Population sortredPopulation = new Population(population.ListOfPossibilities, countOfObiects);
        //    Random r = new Random();

        //    double sumFitness = 0;

        //    foreach (Chromosome chr in population.CurrentPopulation)
        //    {
        //        sumFitness += chr.Fitness;
        //    }

        //    if (maxymalization == true)
        //    {
        //        for (int i = 0; i < population.CurrentPopulation.Count; i++)
        //        {
        //            population.CurrentPopulation[i].SelectionPropability = population.CurrentPopulation[i].Fitness / sumFitness;
        //        }
        //    }
        //    else
        //    {
        //        Chromosome worstChromosome = population.CurrentPopulation[0];

        //        foreach (Chromosome chr in population.CurrentPopulation)
        //        {
        //            if (chr.Fitness < worstChromosome.Fitness)
        //            {
        //                worstChromosome = chr;
        //            }
        //        }

        //        for (int i = 0; i < population.CurrentPopulation.Count; i++)
        //        {
        //            population.CurrentPopulation[i].SelectionPropability = (worstChromosome.Fitness - population.CurrentPopulation[i].Fitness + 1) / (sumFitness + 1);
        //        }
        //    }


        //    //Sortujemy prawdopodobieństwa
        //    sortredPopulation.CurrentPopulation = population.CurrentPopulation.OrderBy(x => x.SelectionPropability).ToList();

        //    //musimy wybrać tyle osobników ile ma być w populacji
        //    for (int i = 0; i < population.CurrentPopulation.Count; i++)
        //    {
        //        double number = r.NextDouble();

        //        //wybieramy osobnika z wylosowanym prawdopodobienstwem
        //        for (int j = 0; j < sortredPopulation.CurrentPopulation.Count; j++)
        //        {
        //            if (j < sortredPopulation.CurrentPopulation.Count -1 )
        //            {
        //                if (number > sortredPopulation.CurrentPopulation[j].SelectionPropability
        //                    && number < sortredPopulation.CurrentPopulation[j + 1].SelectionPropability)
        //                {
        //                    finalPopulation.CurrentPopulation.Add(sortredPopulation.CurrentPopulation[j]);
        //                }
        //            }
        //            else
        //            {
        //                finalPopulation.CurrentPopulation.Add(sortredPopulation.CurrentPopulation[j]);
        //            }
        //        }
        //    }

        //    return finalPopulation;
        //}

        public Population bestMomentRun(Population population, int countOfObiects)
        {
            Random r = new Random();
            Population finalPopulation = new Population(population.ListOfPossibilities, countOfObiects);
            Population sortredPopulation = new Population(population.ListOfPossibilities, countOfObiects);

            //Sortujemy prawdopodobieństwa
            sortredPopulation.CurrentPopulation = population.CurrentPopulation.OrderByDescending(x => x.Fitness).ToList();

            for (int i = 0; i < sortredPopulation.CurrentPopulation.Count; i++)
            {
                if (i < sortredPopulation.CurrentPopulation.Count / 2)
                {
                    finalPopulation.CurrentPopulation.Add(sortredPopulation.CurrentPopulation[i]);
                }
                else
                {
                    Chromosome newChromosome = new Chromosome(population.CurrentPopulation[0].Genes.Count());

                    for (int j = 0; j < population.CurrentPopulation[0].Genes.Count(); j++)
                    {
                        newChromosome.Genes[j] = population.ListOfPossibilities[j][r.Next(0, population.ListOfPossibilities[j].Count())];
                    }

                    finalPopulation.CurrentPopulation.Add(newChromosome);
                }
            }

            return finalPopulation;
        }
    }
}
