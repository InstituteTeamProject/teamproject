﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorytmGenetycznyWzorceReguł.AlgotytmModel;

namespace AlgorytmGenetycznyWzorceReguł.RuleEvaluation
{
    public class EvaluationRules
    {
        public double[] evaluateRules(List<Chromosome> RulesToEvaluate, DecisionTable currentDT)
        {
            int countOfGens = RulesToEvaluate[0].Genes.Count();

            double[] evaluationTable = new double[RulesToEvaluate.Count];

            int[] ruleIsMaching = new int[RulesToEvaluate.Count];

            int genIsMaching = 0;

            for (int i = 0; i < currentDT.ValidationSet.Count; i++)
            {
                for (int j = 0; j < RulesToEvaluate.Count(); j++)
                {
                    for (int k = 0; k < countOfGens; k++)
                    {
                        if (currentDT.ValidationSet[i].Genes[k] == RulesToEvaluate[j].Genes[k] || RulesToEvaluate[j].Genes[k] == "*")
                        {
                            genIsMaching++;
                        }
                    }

                    if (genIsMaching == countOfGens)
                    {
                        ruleIsMaching[j]++;
                    }
                }
            }


            for (int i = 0; i < evaluationTable.Count(); i++)
            {
                evaluationTable[i] = ruleIsMaching[i] / currentDT.ValidationSet.Count;
            }

            return evaluationTable;
        }

        public double evaluateNewRules(DecisionTable currentDT, List<Chromosome> RulesToEvaluate)
        {
            double procentage = 0;

            List<Chromosome> NewRules = new List<Chromosome>();


            //add
            for (int i = 0; i < currentDT.CurrentDecisionTable.Count; i++)
            {
                Chromosome newCh = new Chromosome(currentDT.CurrentDecisionTable[0].Genes.Count());

                for (int j = 0; j < currentDT.CurrentDecisionTable[0].Genes.Count(); j++)
                {
                    if (j == 0)
                    {
                        newCh.Genes[j] = "";
                    }
                    else
                    {
                        newCh.Genes[j] = currentDT.CurrentDecisionTable[i].Genes[j];
                    }
                }

                NewRules.Add(newCh);
            }



            //add decision
            int counter = 0;
            int superCount = RulesToEvaluate[0].Genes.Count() - 1;

            for (int i = 0; i < NewRules.Count; i++)
            {
                for (int j = 0; j < RulesToEvaluate.Count; j++)
                {
                    for (int k = 1; k < RulesToEvaluate[0].Genes.Count(); k++)
                    {
                        if (NewRules[i].Genes[k] == RulesToEvaluate[j].Genes[k] || RulesToEvaluate[j].Genes[k] == "*")
                        {
                            counter++;
                        }
                    }

                    if (counter >= 0.70 * superCount)
                    {
                        NewRules[i].Genes[0] = RulesToEvaluate[j].Genes[0];
                    }
                    counter = 0;
                }
            }


            //delete unclasificated chromosomes
            for (int i = 0; i < NewRules.Count; i++)
            {
                if (NewRules[i].Genes[0] == "")
                {
                    NewRules.RemoveAt(i);
                }
            }




            int goodGenCounter = 0;
            int goodRuleCounter = 0;



            for (int i = 0; i < NewRules.Count; i++)
            {
                for (int j = 0; j < currentDT.CurrentDecisionTable.Count; j++)
                {
                    for (int k = 0; k < NewRules[0].Genes.Count(); k++)
                    {
                        if (NewRules[i].Genes[k] == currentDT.CurrentDecisionTable[j].Genes[k] || NewRules[i].Genes[k] == "*")
                        {
                            goodGenCounter++;
                        }
                    }
                    if (goodGenCounter == superCount)
                    {
                        goodRuleCounter++;
                    }
                }
            }

            return procentage = goodRuleCounter / NewRules.Count;
        }
    }
}
