﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AlgorytmGenetycznyWzorceReguł.AlgotytmModel
{
    enum TableSets
    {
        DecisionTable,
        TreningTable,
        CheckingTable,
        ValidationTable
    };

    public class DecisionTable
    {
        public List<Chromosome> CurrentDecisionTable { get; set; }
        public List<Chromosome> TreningSet { get; set; }
        public List<Chromosome> CheckingSet { get; set; }
        public List<Chromosome> ValidationSet { get; set; }

        public List<List<String>> ListOfPossibilities { get; set; }

        public DecisionTable()
        {
            CurrentDecisionTable = new List<Chromosome>();
            TreningSet = new List<Chromosome>();
            CheckingSet = new List<Chromosome>();
            ValidationSet = new List<Chromosome>();
            ListOfPossibilities = new List<List<String>>();
        }

        //Działa jeśli zakładamy że każda regóła jest w nowej lini oraz geny oddzielone są przecinkami
        public void init(string file)
        {
            string line;
            int countOfGens = 1; //1 bo zawsze będzie 1 więcej aniżeli "," w lini

            //Odczytanie z pliku
            if (File.Exists(file))
            {
                FileStream fs1 = new FileStream(file, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                StreamReader re = new StreamReader(fs1);
                string seperationChar = ",";
                char[] charSeparators = new char[] { ',' };

                //Przeliczanie liczby chromosomów
                line = re.ReadLine();
                for (int i = 0; i < line.Count(); i++)
                {
                    if (line[i].ToString().Contains(seperationChar))
                    {
                        countOfGens++;
                    }
                }

                re.BaseStream.Position = 0;
                re.DiscardBufferedData();

                Chromosome currentChromosome = new Chromosome(countOfGens);
                string currentGene = "";
                int currentGeneNum = 0; //zerujemy wartość aby na tej zmiennej teraz dodawać po kolei geny

                while ((line = re.ReadLine()) != null)
                {
                    for (int i = 0; i < line.Length; i++)
                    {
                        if (line[i].ToString().Contains(seperationChar))
                        {
                            currentChromosome.Genes[currentGeneNum] = currentGene;
                            currentGene = "";
                            currentGeneNum++;
                        }
                        else
                        {
                            currentGene += line[i];
                        }
                    }
                    currentChromosome.Genes[currentGeneNum] = currentGene;
                    currentGene = "";
                    CurrentDecisionTable.Add(currentChromosome);
                    currentChromosome = new Chromosome(countOfGens);
                    currentGeneNum = 0;
                }
                re.Close();

                SetPosibilities();
                SetTreiningAndCheckingSet();
            }
        }

        public void initThreeSetsFromFile(string DtFile, string CheckFile, string ValFile)
        {
            SetOneOfSets(DtFile, TableSets.DecisionTable);
            SetOneOfSets(CheckFile, TableSets.CheckingTable);
            SetOneOfSets(ValFile, TableSets.ValidationTable);

            SetPosibilities();
        }

        private void SetOneOfSets(string file, TableSets whatSet)
        {
            string line;
            int countOfGens = 1; //1 bo zawsze będzie 1 więcej aniżeli "," w lini

            //Odczytanie z pliku
            if (File.Exists(file))
            {
                FileStream fs1 = new FileStream(file, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                StreamReader re = new StreamReader(fs1);
                string seperationChar = ",";
                char[] charSeparators = new char[] { ',' };

                //Przeliczanie liczby chromosomów
                line = re.ReadLine();
                for (int i = 0; i < line.Count(); i++)
                {
                    if (line[i].ToString().Contains(seperationChar))
                    {
                        countOfGens++;
                    }
                }

                re.BaseStream.Position = 0;
                re.DiscardBufferedData();

                Chromosome currentChromosome = new Chromosome(countOfGens);
                string currentGene = "";
                int currentGeneNum = 0; //zerujemy wartość aby na tej zmiennej teraz dodawać po kolei geny

                while ((line = re.ReadLine()) != null)
                {
                    for (int i = 0; i < line.Length; i++)
                    {
                        if (line[i].ToString().Contains(seperationChar))
                        {
                            currentChromosome.Genes[currentGeneNum] = currentGene;
                            currentGene = "";
                            currentGeneNum++;
                        }
                        else
                        {
                            currentGene += line[i];
                        }
                    }
                    currentChromosome.Genes[currentGeneNum] = currentGene;
                    currentGene = "";

                    if (whatSet == TableSets.DecisionTable)
                    {
                        CurrentDecisionTable.Add(currentChromosome);
                    }
                    else if (whatSet == TableSets.CheckingTable)
                    {
                        CheckingSet.Add(currentChromosome);
                    }
                    else if (whatSet == TableSets.ValidationTable)
                    {
                        ValidationSet.Add(currentChromosome);
                    }

                    currentChromosome = new Chromosome(countOfGens);
                    currentGeneNum = 0;
                }
                re.Close();
            }
        }

        private void SetPosibilities()
        {
            for (int i = 0; i < CurrentDecisionTable[0].Genes.Count(); i++)
            {
                ListOfPossibilities.Add(new List<string>());
            }

            for (int i = 0; i < CurrentDecisionTable.Count; i++)
            {
                for (int j = 0; j < CurrentDecisionTable[i].Genes.Count(); j++)
                {
                    if (CurrentDecisionTable[i].Genes[j] != "null")
                    {
                        if (!ListOfPossibilities[j].Contains(CurrentDecisionTable[i].Genes[j]))
                        {
                            ListOfPossibilities[j].Add(CurrentDecisionTable[i].Genes[j]);
                        }
                    }
                }
            }
        }

        private void SetTreiningAndCheckingSet()
        {
            Random r = new Random();
            //setTrening
            for (int i = 0; i < (int)(CurrentDecisionTable.Count() * 0.3); i++)
            {
                TreningSet.Add(CurrentDecisionTable[r.Next(0, CurrentDecisionTable.Count())]);
            }
            //setChecking
            for (int i = 0; i < (int)(CurrentDecisionTable.Count() * 0.5); i++)
            {
                CheckingSet.Add(CurrentDecisionTable[i]);
            }
        }
    }
}
