﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmGenetycznyWzorceReguł.AlgotytmModel
{
    public class Chromosome : IComparable<Chromosome>
    {
        public string[] Genes { get; set; }
        public double Fitness { get; set; }
        public double SelectionPropability { get; set; }

        public Chromosome(int countOfgens)
        {
            Genes = new string[countOfgens];
        }

        public void mutate(List<List<string>> ListOfPossibilities)
        {
            Random r = new Random();
            int iterations = (int)(Genes.Count() * 0.35);

            for (int i = 0; i < iterations; i++)
            {
                int columnOfGen = r.Next(0, Genes.Count());

                Genes[columnOfGen] = ListOfPossibilities[columnOfGen][r.Next(0, ListOfPossibilities[columnOfGen].Count())];
            }
        }

        public void calculateFitness(List<List<string>> ListOfPossibilities, int countOfObiects, DecisionTable decisionTable, List<Chromosome> CurrentPopulation)
        {
            int GenesX = 0;
            
            for (int i = 0; i < Genes.Count(); i++ )
            { 
                if (Genes[i] == "*")
                {
                    GenesX++;
                }
            }

            double liczbaAtrr = Genes.Count();
            double bezX = Genes.Count() - GenesX;
            double nosnik = support(decisionTable);
            double liczbaObiektow = decisionTable.CheckingSet.Count();

            if (bezX > 0 & nosnik > 0)
            {
                bezX = 0.1;
            }
            Fitness = (bezX / liczbaAtrr) * (nosnik / liczbaObiektow);          
        }

        //ToDo prawidlowe liczenie confidence ?!
        //public double confidence(DecisionTable decisionTable)
        //{
        //    double _confidence = 0;
        //    double A = 0;

        //    double matchToRow = 0;

        //    for (int i = 0; i < decisionTable.CheckingSet.Count; i++)
        //    {
        //        for (int j = 0; j < Genes.Count(); j++)
        //        {
        //            if (decisionTable.CheckingSet[i].Genes[j] == Genes[j] || Genes[j] == "*")
        //            {
        //                A++;
        //            }
        //        }
        //        if (A == Genes.Count())
        //        {
        //            matchToRow++;
        //        }
        //        A = 0;
        //    }
        //    return _confidence = matchToRow;
        //}

        //nosnik wzorca
        public double support(DecisionTable decisionTable)
        {
            double _support = 0;
            double A = 0;
            double matchToRow = 0;

            for (int i = 0; i < decisionTable.CheckingSet.Count; i++)
            {
                for (int j = 0; j < Genes.Count(); j++)
                {
                    if (decisionTable.CheckingSet[i].Genes[j] == Genes[j] || Genes[j] == "*")
                    {
                        A++;
                    }
                }
                if (A == Genes.Count())
                {
                    matchToRow++;
                }
                A = 0;
            }
            return _support = matchToRow / decisionTable.CheckingSet.Count;
        }

        public int CompareTo(Chromosome obj)
        {
            return Fitness.CompareTo(obj.Fitness);
        }

    }
}
