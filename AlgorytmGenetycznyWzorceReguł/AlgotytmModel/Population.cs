﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmGenetycznyWzorceReguł.AlgotytmModel
{
    public class Population
    {
        public List<Chromosome> CurrentPopulation { get; set; }
        public List<List<string>> ListOfPossibilities { get; set; }
        public int countOfObiects;

        public Population(List<List<string>> listOfPossibilities, int countOfObiects)
        {
            CurrentPopulation = new List<Chromosome>();
            ListOfPossibilities = listOfPossibilities;
            this.countOfObiects = countOfObiects;
        }

        public void init(List<List<string>> ListOfPossibilities, int populationRange, int chromosomeCount )
        {
            Random r = new Random();

            for (int i = 0; i < populationRange; i++)
            {
                Chromosome initialChromosome = new Chromosome(chromosomeCount);

                for (int j = 0; j < chromosomeCount; j++)
                {
                    initialChromosome.Genes[j] = "*";

                    double x = r.NextDouble();
                    if (x > 0.90)
                    {                 
                            initialChromosome.Genes[j] = ListOfPossibilities[j][r.Next(0, ListOfPossibilities[j].Count())];                       
                    }
                }

                CurrentPopulation.Add(initialChromosome);
            }
        }

        public void evaluate(DecisionTable decisionTable)
        {
            foreach (Chromosome chrom in CurrentPopulation)
            {
                chrom.calculateFitness(ListOfPossibilities, countOfObiects, decisionTable, CurrentPopulation);
            }
        }
    }
}
