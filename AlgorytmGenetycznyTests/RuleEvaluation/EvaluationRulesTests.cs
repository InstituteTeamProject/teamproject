﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgorytmGenetycznyWzorceReguł.RuleEvaluation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorytmGenetycznyWzorceReguł.AlgotytmModel;

namespace AlgorytmGenetycznyWzorceReguł.RuleEvaluation.Tests
{
    [TestClass()]
    public class EvaluationRulesTests
    {
        [TestMethod()]
        public void evaluateRulesTest()
        {
            string[] Genes = new string[] { "1", "*", "*", "4", "5" };
            string[] Genes1 = new string[] { "*", "2", "4", "*", "*" };

            string[] Genes2 = new string[] { "1", "2", "3", "4", "5" };
            string[] Genes3 = new string[] { "1", "2", "3", "4", "5" };

            Chromosome chrom1 = new Chromosome(5);
            chrom1.Genes = Genes;

            Chromosome chrom2 = new Chromosome(5);
            chrom1.Genes = Genes1;

            Chromosome chrom3 = new Chromosome(5);
            chrom1.Genes = Genes2;

            Chromosome chrom4 = new Chromosome(5);
            chrom1.Genes = Genes3;

           List<Chromosome> RulesToEvaluate = new List<Chromosome>();
           DecisionTable currentDT = new DecisionTable();

           RulesToEvaluate.Add(chrom1);
           RulesToEvaluate.Add(chrom2);

           currentDT.ValidationSet.Add(chrom3);
           currentDT.ValidationSet.Add(chrom4);

           EvaluationRules evrule = new EvaluationRules();
           evrule.evaluateRules(RulesToEvaluate, currentDT);

           try
           {
               evrule.evaluateRules(RulesToEvaluate, currentDT);      
                   Assert.IsTrue(1 == 1);             
           }
           catch (Exception ex)
           {
               Assert.Fail("Some Excepction Shown! Excepction:" + ex);
           }
        }
    }
}