﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgorytmGenetycznyWzorceReguł.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorytmGenetycznyWzorceReguł.AlgotytmModel;

namespace AlgorytmGenetycznyWzorceReguł.Controller.Tests
{
    [TestClass()]
    public class SelectionTests
    {
        [TestMethod()]
        public void turnamentRunTest()
        {

            List<List<string>> possibilitiesTest = new List<List<string>>();

            possibilitiesTest.Add(new List<string>());
            possibilitiesTest.Add(new List<string>());
            possibilitiesTest.Add(new List<string>());
            possibilitiesTest.Add(new List<string>());
            possibilitiesTest.Add(new List<string>()); 

            possibilitiesTest[0].Add("1");
            possibilitiesTest[0].Add("2");
            possibilitiesTest[0].Add("3");
            possibilitiesTest[0].Add("4");
            possibilitiesTest[0].Add("5");

            possibilitiesTest[1].Add("6");
            possibilitiesTest[1].Add("7");
            possibilitiesTest[1].Add("8");
            possibilitiesTest[1].Add("9");
            possibilitiesTest[1].Add("10");

            possibilitiesTest[2].Add("1");
            possibilitiesTest[2].Add("2");
            possibilitiesTest[2].Add("3");
            possibilitiesTest[2].Add("4");
            possibilitiesTest[2].Add("5");

            possibilitiesTest[3].Add("6");
            possibilitiesTest[3].Add("7");
            possibilitiesTest[3].Add("8");
            possibilitiesTest[3].Add("9");
            possibilitiesTest[3].Add("10");

            possibilitiesTest[4].Add("1");
            possibilitiesTest[4].Add("2");
            possibilitiesTest[4].Add("3");
            possibilitiesTest[4].Add("4");
            possibilitiesTest[4].Add("5");
            

            string[] Genes = new string[] { "1", "2", "3", "4", "5" };
            string[] Genes1 = new string[] { "*", "2", "4", "*", "*" };
            string[] Genes2 = new string[] { "1", "*", "5", "2", "2" };
            string[] Genes3 = new string[] { "1", "2", "8", "4", "5" };
            string[] Genes4 = new string[] { "1", "*", "9", "*", "5" };

            DecisionTable CurrentPopulation = new DecisionTable();
            List<Chromosome> CheckingSet = new List<Chromosome>();

            Chromosome chrom1 = new Chromosome(5);
            chrom1.Genes = Genes;

            Chromosome chrom2 = new Chromosome(5);
            chrom1.Genes = Genes1;

            Chromosome chrom3 = new Chromosome(5);
            chrom1.Genes = Genes2;

            Chromosome chrom4 = new Chromosome(5);
            chrom1.Genes = Genes3;

            Chromosome chrom5 = new Chromosome(5);
            chrom1.Genes = Genes4;

            CurrentPopulation.CheckingSet.Add(chrom1);
            CurrentPopulation.CheckingSet.Add(chrom2);
            CurrentPopulation.CheckingSet.Add(chrom3);
            CurrentPopulation.CheckingSet.Add(chrom4);
            CurrentPopulation.CheckingSet.Add(chrom5);



            Population finalPopulation = new Population(possibilitiesTest, possibilitiesTest.Count());
            Random r = new Random();

            int populationCount = CurrentPopulation.CheckingSet.Count();

            Chromosome bestChromosome;
            Chromosome[] tmpArray = new Chromosome[4];

            for (int i = 0; i < populationCount; i++)
            {
                tmpArray[0] = CurrentPopulation.CheckingSet[r.Next(0, populationCount)];
                tmpArray[1] = CurrentPopulation.CheckingSet[r.Next(0, populationCount)];
                tmpArray[2] = CurrentPopulation.CheckingSet[r.Next(0, populationCount)];
                tmpArray[3] = CurrentPopulation.CheckingSet[r.Next(0, populationCount)];

                bestChromosome = tmpArray[0];

                for (int j = 0; j < 4; j++)
                {
                    if (tmpArray[j].Fitness > bestChromosome.Fitness)
                    {
                        bestChromosome = tmpArray[j];
                    }
                }

                finalPopulation.CurrentPopulation.Add(bestChromosome);
            }

            try
            {
                if (finalPopulation.countOfObiects > 0)
                {
                    Assert.IsTrue(1 == 1);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail("Some Excepction Shown! Excepction:" + ex);
            }
        }
      
        [TestMethod()]
        public void bestMomentRunTest()
        {
            List<List<string>> possibilitiesTest = new List<List<string>>();

            possibilitiesTest.Add(new List<string>());
            possibilitiesTest.Add(new List<string>());
            possibilitiesTest.Add(new List<string>());
            possibilitiesTest.Add(new List<string>());
            possibilitiesTest.Add(new List<string>());

            possibilitiesTest[0].Add("1");
            possibilitiesTest[0].Add("2");
            possibilitiesTest[0].Add("3");
            possibilitiesTest[0].Add("4");
            possibilitiesTest[0].Add("5");

            possibilitiesTest[1].Add("6");
            possibilitiesTest[1].Add("7");
            possibilitiesTest[1].Add("8");
            possibilitiesTest[1].Add("9");
            possibilitiesTest[1].Add("10");

            possibilitiesTest[2].Add("1");
            possibilitiesTest[2].Add("2");
            possibilitiesTest[2].Add("3");
            possibilitiesTest[2].Add("4");
            possibilitiesTest[2].Add("5");

            possibilitiesTest[3].Add("6");
            possibilitiesTest[3].Add("7");
            possibilitiesTest[3].Add("8");
            possibilitiesTest[3].Add("9");
            possibilitiesTest[3].Add("10");

            possibilitiesTest[4].Add("1");
            possibilitiesTest[4].Add("2");
            possibilitiesTest[4].Add("3");
            possibilitiesTest[4].Add("4");
            possibilitiesTest[4].Add("5");

            Random r = new Random();
            Population finalPopulation = new Population(possibilitiesTest, possibilitiesTest.Count());
            Population sortredPopulation = new Population(possibilitiesTest, possibilitiesTest.Count());
            Population population = new Population(possibilitiesTest, possibilitiesTest.Count());

            //Sortujemy prawdopodobieństwa
            sortredPopulation.CurrentPopulation = population.CurrentPopulation.OrderByDescending(x => x.Fitness).ToList();

            for (int i = 0; i < sortredPopulation.CurrentPopulation.Count; i++)
            {
                if (i < sortredPopulation.CurrentPopulation.Count / 2)
                {
                    finalPopulation.CurrentPopulation.Add(sortredPopulation.CurrentPopulation[i]);
                }
                else
                {
                    Chromosome newChromosome = new Chromosome(population.CurrentPopulation[0].Genes.Count());

                    for (int j = 0; j < population.CurrentPopulation[0].Genes.Count(); j++)
                    {
                        newChromosome.Genes[j] = population.ListOfPossibilities[j][r.Next(0, population.ListOfPossibilities[j].Count())];
                    }

                    finalPopulation.CurrentPopulation.Add(newChromosome);
                }
            }

            try
            {
                if (finalPopulation.countOfObiects > 0)
                {
                    Assert.IsTrue(1 == 1);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail("Some Excepction Shown! Excepction:" + ex);
            }
           
        }
    }
}