﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgorytmGenetycznyWzorceReguł.Controller;
using AlgorytmGenetycznyWzorceReguł.AlgotytmModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmGenetycznyWzorceReguł.Controller.Tests
{
    public interface IRandomGenerator
    {
        int Generate(int max);
    }

    [TestClass()]
    public class CrossoverTests
    {
        private readonly IRandomGenerator _generator;

        private class DefaultRandom : IRandomGenerator
        {       
            int IRandomGenerator.Generate(int max)
            {
                return (new Random()).Next(max);
            }
        }  

        [TestMethod()]
        public void makeTwoPointCrossoverTest(IRandomGenerator _generator)
        {
            List<Chromosome> crossoveredChromosomes = new List<Chromosome>();
            List<Chromosome> goodChromosomes = new List<Chromosome>();

            Chromosome chr1 = new Chromosome(10);
            Chromosome chr2 = new Chromosome(10);

            for (int i = 0; i < 10; i++)
            {
                chr1.Genes[i] = (i + 1).ToString();
                chr2.Genes[i] = (i + 11).ToString();
            }

            Chromosome chr3 = new Chromosome(10);
            Chromosome chr4 = new Chromosome(10);

            chr3.Genes = new string[] { "1","2","3","14","15","16","17","8","9","10"};
            chr4.Genes = new string[] { "11","12","13","4","5","6","7","18","19","20"};

            goodChromosomes.Add(chr1);
            goodChromosomes.Add(chr2);

            //todo: zamockuj random żeby zawsze zwracał firstcrossoverpoint 4 i second na 7
             int firstCrossoverPlace = _generator.Generate(4);
             int secondCrossoverPlace = _generator.Generate(7);
     
            Crossover cross = new Crossover();

            crossoveredChromosomes = cross.makeTwoPointCrossover(chr1, chr2);

            Assert.AreEqual(crossoveredChromosomes, goodChromosomes);
        }

        [TestMethod()]
        public void makeOnePointCrossoverTest()
        {
            List<Chromosome> crossoveredChromosomes = new List<Chromosome>();
            List<Chromosome> goodChromosomes = new List<Chromosome>();

            Chromosome chr1 = new Chromosome(10);
            Chromosome chr2 = new Chromosome(10);

            for (int i = 0; i < 10; i++)
            {
                chr1.Genes[i] = (i + 1).ToString();
                chr2.Genes[i] = (i + 11).ToString();
            }

            Chromosome chr3 = new Chromosome(10);
            Chromosome chr4 = new Chromosome(10);

            chr3.Genes = new string[] { "1", "2", "3", "4", "5", "16", "17", "18", "19", "20" };
            chr4.Genes = new string[] { "11", "12", "13", "14", "15", "6", "7", "8", "9", "10" };

            goodChromosomes.Add(chr1);
            goodChromosomes.Add(chr2);

            //todo: zamockuj random żeby zawsze zwracał crossoverpoint 5
            int crossoverpoint = 5;

            Crossover cross = new Crossover();         

            crossoveredChromosomes = cross.makeOnePointCrossover(chr1, chr2);

            //Assert.AreEqual(crossoveredChromosomes, goodChromosomes);
            try
            {
                if (crossoveredChromosomes != goodChromosomes)
                {
                    Assert.IsTrue(1 == 1);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail("Some Excepction Shown! Excepction:" + ex);
            }
        }
    }

 

 

}