﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgorytmGenetycznyWzorceReguł.AlgotytmModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmGenetycznyWzorceReguł.AlgotytmModel.Tests
{
    [TestClass()]
    public class DecisionTableTests
    {
        [TestMethod()]
        public void initDecisionTableTest()
        {
            List<Chromosome> TestDecisionTable = new List<Chromosome>();

            Chromosome a = new Chromosome(5);
            a.Genes[0] = "5.1";
            a.Genes[1] = "3.5";
            a.Genes[2] = "1.4";
            a.Genes[3] = "0.2";
            a.Genes[4] = "Iris-setosa";

            Chromosome b = new Chromosome(5);
            b.Genes[0] = "5.9";
            b.Genes[1] = "3.0";
            b.Genes[2] = "5.1";
            b.Genes[3] = "1.8";
            b.Genes[4] = "Iris-virginica";

            Chromosome c = new Chromosome(5);
            c.Genes[0] = "4.7";
            c.Genes[1] = "3.2";
            c.Genes[2] = "1.3";
            c.Genes[3] = "0.2";
            c.Genes[4] = "Iris-setosa";

            TestDecisionTable.Add(a);
            TestDecisionTable.Add(b);
            TestDecisionTable.Add(c);

            DecisionTable dt = new DecisionTable();
            string file = System.IO.Path.Combine(Environment.CurrentDirectory, "Resources", "irisTest.txt");
            dt.init(file);

            bool thesameObiect = false;

            if (dt.CurrentDecisionTable[0].Genes[2] == TestDecisionTable[0].Genes[2]
                && dt.CurrentDecisionTable[2].Genes[3] == TestDecisionTable[2].Genes[3])
            {
                thesameObiect = true;
            }

            Assert.IsTrue(thesameObiect);
        }

        [TestMethod()]
        public void initThreeSetsFromFileTest()
        {
            string DtFile = System.IO.Path.Combine(Environment.CurrentDirectory, "Resources", "irisTest.txt");
            string TrainFile = System.IO.Path.Combine(Environment.CurrentDirectory, "Resources", "irisTest.txt");
            string CheckFile = System.IO.Path.Combine(Environment.CurrentDirectory, "Resources", "irisTest.txt");
            string ValFile = System.IO.Path.Combine(Environment.CurrentDirectory, "Resources", "irisTest.txt");
            DecisionTable dt = new DecisionTable();
       
            try
            {
                //dt.initThreeSetsFromFile(DtFile, TrainFile, CheckFile, ValFile);
                dt.initThreeSetsFromFile(DtFile, CheckFile, ValFile);
                Assert.IsTrue(1==1);     
            }
            catch (Exception ex)
            {
                Assert.Fail("Some Excepction Shown! Excepction:"+ex);
            }
        }
    }
}