﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgorytmGenetycznyWzorceReguł.AlgotytmModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmGenetycznyWzorceReguł.AlgotytmModel.Tests
{
    [TestClass()]
    public class ChromosomeTests
    {
        [TestMethod()]
        public void mutateTest()
        {
            List<List<string>> possibilitiesTest = new List<List<string>>();

            string[] Genes1 = new string[] { "1", "2", "3", "4", "5" };
            string[] Genes2 = Genes1;
            List<string> ListOfPossibilities = new List<string> {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };

            Random r = new Random();
            int iterations = (int)(5 * 0.35);

            for (int i = 0; i < iterations; i++)
            {
                int columnOfGen = r.Next(0, Genes1.Count());

                Genes1[columnOfGen] = ListOfPossibilities[r.Next(0, ListOfPossibilities[columnOfGen].Count())];
            }

            try
            {
                if (Genes1 != Genes2)
                {
                    Assert.IsTrue(1 == 1);
                }

            }
            catch (Exception ex)
            {
                Assert.Fail("Some Excepction Shown! Excepction:" + ex);
            }
        }

        [TestMethod()]
        public void calculateFitnessTest()
        {
            int GenesX = 0;

            string[] Genes = new string[] { "1", "2", "3", "4", "5" };
            string[] Genes1 = new string[] { "*", "2", "4", "*", "*" };
            string[] Genes2 = new string[] { "1", "*", "5", "2", "2" };
            string[] Genes3 = new string[] { "1", "2", "8", "4", "5" };
            string[] Genes4 = new string[] { "1", "*", "9", "*", "5" };

            DecisionTable decisionTable = new DecisionTable();

            for (int i = 0; i < Genes.Count(); i++)
            {
                if (Genes[i] == "*")
                {
                    GenesX++;
                }
                if (Genes1[i] == "*")
                {
                    GenesX++;
                }
                if (Genes2[i] == "*")
                {
                    GenesX++;
                }
                if (Genes3[i] == "*")
                {
                    GenesX++;
                }
                if (Genes4[i] == "*")
                {
                    GenesX++;
                }
            }

            double liczbaAtrr = Genes.Count();
            double bezX = Genes.Count() - GenesX;
            double nosnik = 0.3;
            double liczbaObiektow = decisionTable.CheckingSet.Count();

            if (bezX > 0 & nosnik > 0)
            {
                bezX = 0.1;
            }
            double Fitness = (bezX / liczbaAtrr) * (nosnik / liczbaObiektow);

            try
            {
                if (Fitness != 0)
                {
                    Assert.IsTrue(1 == 1);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail("Some Excepction Shown! Excepction:" + ex);
            }


        }

        [TestMethod()]
        public void supportTest()
        {
            double _support = 0;
            double A = 0;
            double matchToRow = 0;

            string[] Genes = new string[] { "1", "2", "3", "4", "5" };
            string[] Genes1 = new string[] { "*", "2", "4", "*", "*" };
            string[] Genes2 = new string[] { "1", "*", "5", "2", "2" };
            string[] Genes3 = new string[] { "1", "2", "8", "4", "5" };
            string[] Genes4 = new string[] { "1", "*", "9", "*", "5" };

            DecisionTable decisionTable = new DecisionTable();
            List<Chromosome> CheckingSet = new List<Chromosome>();

            Chromosome chrom1 = new Chromosome(5);
            chrom1.Genes= Genes;

            Chromosome chrom2 = new Chromosome(5);
            chrom1.Genes = Genes1;

            Chromosome chrom3 = new Chromosome(5);
            chrom1.Genes = Genes2;

            Chromosome chrom4 = new Chromosome(5);
            chrom1.Genes = Genes3;

            Chromosome chrom5 = new Chromosome(5);
            chrom1.Genes = Genes4;

            decisionTable.CheckingSet.Add(chrom1);
            decisionTable.CheckingSet.Add(chrom2);
            decisionTable.CheckingSet.Add(chrom3);
            decisionTable.CheckingSet.Add(chrom4);
            decisionTable.CheckingSet.Add(chrom5);

            for (int i = 0; i < CheckingSet.Count; i++)
            {
                for (int j = 0; j < Genes.Count(); j++)
                {
                    if (CheckingSet[i].Genes[j] == Genes[j] || Genes[j] == "*")
                    {
                        A++;
                    }
                }
                if (A == Genes.Count())
                {
                    matchToRow++;
                }
                A = 0;
            }
            _support = matchToRow / CheckingSet.Count;

            try
            {
                if (_support != 0)
                {
                    Assert.IsTrue(1 == 1);
                }
            }
            catch (Exception ex)
            {
                Assert.Fail("Some Excepction Shown! Excepction:" + ex);
            }
        }

        [TestMethod()]
        public void CompareToTest()
        {
            string[] Genes = new string[] { "1", "2", "3", "4", "5" };
            string[] Genes1 = new string[] { "*", "2", "4", "*", "*" };

            List<string> ListOfPossibilities = new List<string> { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };

            Chromosome chrom1 = new Chromosome(5);
            chrom1.Genes = Genes;

            Chromosome chrom2 = new Chromosome(5);
            chrom1.Genes = Genes1;

            chrom1.Fitness = 0.7;
            chrom2.Fitness = 0.3;
           
            Assert.IsTrue(chrom1.CompareTo(chrom2) == 1);
            
            

        }
    }
}