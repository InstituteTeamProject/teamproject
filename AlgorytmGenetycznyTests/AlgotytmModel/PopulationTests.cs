﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlgorytmGenetycznyWzorceReguł.AlgotytmModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorytmGenetycznyWzorceReguł.AlgotytmModel.Tests
{
    [TestClass()]
    public class PopulationTests
    {
        [TestMethod()]
        public void initPopulationTest()
        {
            // mama 1,2
            // tata 1,2
            List<List<string>> possibilitiesTest = new List<List<string>>();

            possibilitiesTest.Add(new List<string>());
            possibilitiesTest.Add(new List<string>());

            possibilitiesTest[0].Add("mama");
            possibilitiesTest[0].Add("1");
            possibilitiesTest[0].Add("2");

            possibilitiesTest[1].Add("tata");
            possibilitiesTest[1].Add("1");
            possibilitiesTest[1].Add("2");

            Population popul = new Population(possibilitiesTest, 2);

            try
            {
                popul.init(possibilitiesTest, 2, 2);
                Assert.IsTrue(1 == 1);
            }
            catch (Exception ex)
            {
                Assert.Fail("Some Excepction Shown! Excepction:" + ex);
            }
        }

        [TestMethod()]
        public void evaluateTest()
        {
            // mama 1,2
            // tata 1,2
            List<List<string>> possibilitiesTest = new List<List<string>>();

            possibilitiesTest.Add(new List<string>());
            possibilitiesTest.Add(new List<string>());

            possibilitiesTest[0].Add("mama");
            possibilitiesTest[0].Add("1");
            possibilitiesTest[0].Add("2");

            possibilitiesTest[1].Add("tata");
            possibilitiesTest[1].Add("1");
            possibilitiesTest[1].Add("2");

            Population popul = new Population(possibilitiesTest, 2);

            DecisionTable dt = new DecisionTable();

            try
            {
                popul.evaluate(dt);
                Assert.IsTrue(1 == 1);
            }
            catch (Exception ex)
            {
                Assert.Fail("Some Excepction Shown! Excepction:" + ex);
            }
        }
    }
}